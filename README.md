# Home Assistant Support by SmartLiving.Rocks

## Add-on zu My.HomeAssistant hinzufügen

[Mit My.HomeAssistant hinzufügen](https://my.home-assistant.io/redirect/_change/?redirect=supervisor_add_addon_repository%2F%3Frepository_url%3Dhttps%253A%252F%252Fgitlab.com%252FSmartLiving.Rocks%252FHomeAssistantSupport)

## SmartLiving Support Service VPN Add-on

Dieses Add-on ermöglicht es Ihnen zu unserem Support Netzwerk einer Verbindung aufzubauen, ohne dass dafür extra Ports auf Ihrem Router aufgemacht werden müssen. Das ist zum einen eine sehr unsichere Methode, besonders für unerfahrene Nutzer. Zum anderen ist es manchmal nicht möglich eine sichere Verbindung aufzubauen z.B. wenn Sie auf Ihrem Boot, Camper, Tiny Haus oder anderen Orten mit mobilem Internet Zugang befinden. 

### OpenVPN

Dieses Add-on basiert auf OpenVPN und baut eine gesicherte Verbindung über ein virtuelles privates Netzwerk zu unserem Support auf. 

### Installation

#### OpenVPN Zugangsdaten hinterlegen (*.ovpn Datei speichern)

Die ovpn Datei, die wir Ihnen zugeschickt haben, müssen Sie in dem Ordner /shares abspeichern. Benutzen Sie hierzu eine von Ihnen bevorzugte Methode wie SMB oder fügen Sie die Datei manuell hinzu, mit File oder Coder. Hier zeigen wir Ihnen, wie es geht:

#### Repository hinzufügen

Kopieren Sie den folgenden Code in die Zwischenablage:

https://github.com/SmartLiving-Rocks/HomeAssistantSupport

Klicken Sie auf Einstellungen dann Add-Ons danach unten rechts in der Ecke auf Add-on Store und abschließend auf die drei Punkte untereinander oben rechts in der Ecke Repositories. Fügen Sie den kopierten Code ein und klicken auf Hinzufügen und dann auf Schließen. Das Add-on ist jetzt zu Home Assistant hinzugefügt und weiter unten in der Add-on-Liste zu finden.

#### Add-on installieren 

Klicken Sie nun auf den Eintrag und es öffnet sich ein Fenster mit weiteren Informationen. Klicken Sie jetzt auf installieren. Nach ein paar Minuten ist das Add-on installiert und Sie können es jetzt starten. Sie haben sich erfolgreich mit unserem Netzwerk verbunden. Zur Überprüfung können Sie auf den Reiter Logs klicken und ggf. Fehler erkennen. 

### Support Administrator einrichten

Damit wir uns mit Ihrere Home Assistant Instanz verbinden können, müssen Sie noch einen Benutzer anlegen und diesem Administrationsrechte vergeben. 

Nutzername: SmartLiving

Passwort: Rocks369!

Wir werden das Passwort direkt nach unserem ersten Login ändern.
